import useSWR from 'swr';

// import productFetcher from 'utils/api/product';
const fetcher = (url) => fetch(url).then((res) => res.json());

export default function usePromoter() {
  const { data, mutate, error } = useSWR('/api/promoters.json', fetcher);

  const loading = !data && !error;
  console.log(data);
  return {
    loading,
    error,
    data: !!data ? data : [],
    mutate,
  };
}
