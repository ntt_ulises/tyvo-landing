import React from 'react';
import dynamic from 'next/dynamic';
import {useRouter} from 'next/router';
import {Modal} from '@redq/reuse-modal';
import {Banner} from 'components/banner/banner';
import StoreNav from 'components/store-nav/store-nav';
import CarouselServices from 'components/carousel/carouselServices';
import BannerGetMobilesApps from 'components/banner/bannerGetMobilesApps';
import {
  OfferServices,
  MobileCarouselDropdown,
} from 'assets/styles/pages.style';
// Static Data Import Here
import {SEO} from 'components/seo';
import {useRefScroll} from 'utils/use-ref-scroll';
import {sitePages} from 'site-settings/site-pages';
import {siteServices} from 'site-settings/site-services';

import {CATEGORY_MENU_ITEMS} from 'site-settings/site-navigation';
import BannerAdverting from "../components/banner/bannerAdvertising";
import Footer from "../layouts/footer";
import Locations from "../components/locations/Locations";
import {BannerPromoter} from "../components/banner/bannerPromoter";

const Sidebar = dynamic(() => import('layouts/sidebar/sidebar'));
const CartPopUp = dynamic(() => import('features/carts/cart-popup'), {
  ssr: false,
});

const CategoryPage: React.FC<any> = ({deviceType}) => {
  const {query} = useRouter();
  const {elRef: targetRef, scroll} = useRefScroll({
    percentOfElement: 0,
    percentOfContainer: 0,
    offsetPX: -110,
  });
  
  React.useEffect(() => {
    if (query.text || query.category) {
      scroll();
    }
  }, [query.text, query.category]);
  
  const PAGE_TYPE: any = query.type;
  const page = sitePages[PAGE_TYPE];
  if (!page) return null;
  return <>
      <SEO title={page.page_title} description={page.page_description}/>
      
      <Modal>
        <Banner
          intlTitleId={page.banner_title_id}
          intlDescriptionId={page.banner_description_id}
          imageUrl={page.banner_image_url}
        />
        <MobileCarouselDropdown>
          <StoreNav items={CATEGORY_MENU_ITEMS}/>
          <Sidebar type={PAGE_TYPE} deviceType={deviceType}/>
        </MobileCarouselDropdown>
        <OfferServices>
          <div>
            <CarouselServices data={siteServices}/>
          </div>
        </OfferServices>
        <BannerGetMobilesApps deviceType={deviceType}/>
        <BannerAdverting/>
        <CartPopUp deviceType={deviceType}/>
        <Locations/>
        <BannerPromoter/>
        <div>
          <img src="https://i.imgur.com/8EIotVX.png" width='100%' alt="altavoz"/>
        </div>
        <Footer/>
      </Modal>
    </>
};

export default CategoryPage;
