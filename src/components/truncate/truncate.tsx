import React, {useState} from 'react';

type ReadMoreProps = {
  more?: string;
  less?: string;
  character?: number;
};

const ReadMore = ({children, more, less, character}) => {
  const [expanded, setExpanded] = useState(false);
  
  const toggleLines = (event) => {
    event.preventDefault();
    setExpanded(!expanded);
  };
  
  if (!children) return null;
  
  return (
    <>
      {(children && children.length < character) || expanded
        ? children
        : children.substring(0, character)}
      {children && children.length > character && !expanded && (
        <>
          <br/>
          <span>
            <a
              href="#"
              onClick={toggleLines}
              style={{color: '#009e7f', fontWeight: 'bold'}}
            >
              {more}
            </a>
          </span>
        </>
      )}
      {children && children.length > character && expanded && (
        <>
          <br/>
          <span>
            <a
              href="#"
              onClick={toggleLines}
              style={{color: '#009e7f', fontWeight: 'bold'}}
            >
              {less}
            </a>
          </span>
        </>
      )}
    </>
  );
};

export const TextTruncate = ({children, character, ...rest}) => {
  if (!children) return null;
  return <>
    {
      (children && children.length < character) ?
        <span {...rest}>{children}</span> :
        <span {...rest}>{children.substring(0, character - 3)}...</span>
    }
  </>
};

ReadMore.defaultProps = {
  character: 150,
  more: 'Read more',
  less: 'less',
};

export default ReadMore;
