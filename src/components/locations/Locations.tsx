import React from 'react';
import {Col, Grid, Row} from "react-styled-flexboxgrid";
import locationsData from "./locationsData";

const Locations = (props: IProps) => {
  return <div style={{padding: 20, background: '#F7F7F7'}}>
    <Grid style={{width: '100%'}}>
      <Row>
        <Col xs={12} style={{paddingBottom: 10}}><h3>Otras ciudades</h3></Col>
        {
          locationsData.map(department => {
            return <Col xs={6} md={4} key={department}><p>{department}</p></Col>
          })
        }
      </Row>
    </Grid>
  </div>
}

interface IProps {

}

export default Locations;