import React from 'react';
import imgAppStore from '../../assets/images/banner/banner-market.png';
import {BannerContainerAdvertising, Button, BannerContainerAdvertisingResponsive} from './banner.style';

const BannerAdverting = () => {
  return <BannerContainerAdvertisingResponsive>
    <BannerContainerAdvertising style={{backgroundImage: `url('${imgAppStore}')`}}>
      <div>
        <h3 style={{fontSize: 'calc(18px + .5vw)'}}>Supermercado</h3>
        <Button>
          Online
        </Button>
      </div>
    </BannerContainerAdvertising>
  </BannerContainerAdvertisingResponsive>
}

export default BannerAdverting