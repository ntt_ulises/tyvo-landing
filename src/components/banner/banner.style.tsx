import styled from 'styled-components';
import {background} from 'styled-system';
import css from '@styled-system/css';

export const Box = styled.div(
  css({
    height: [200, '85vh'],
  }),
  {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    position: 'relative',
    // backgroundColor: #f7f7f7;
  }
);
export const Image = styled.div<any>(
  css({
    backgroundSize: ['cover'],
  }),
  {
    width: '100%',
    height: '100%',
    backgroundPosition: 'left center',
    backgroundRepeat: 'no-repeat',
    position: 'absolute',
    top: 0,
    left: 0,
    '@media (max-width: 990px) and (min-width: 768px)': {
      backgroundPosition: 'inherit',
    },
  },
  background
);

export const Content = styled.div(
  css({
    px: ['15px'],
    pt: [40, 0],
  }),
  {
    position: 'relative',
    zIndex: 2,
  }
);
export const Title = styled.h2(
  css({
    fontSize: [17, '2xl', 45],
    color: 'text.bold',
    fontWeight: 'bold',
  }),
  {
    marginBottom: 15,
    textAlign: 'center',
  }
);
export const Description = styled.p(
  css({
    fontSize: ['base', 'md'],
    color: 'text.regular',
    marginBottom: [null, 60],
    display: ['none', 'block'],
    fontWeight: 'regular',
    lineHeight: 'body',
  }),
  {
    textAlign: 'center',
  }
);

export const SearchWrapper = styled.div(
  css({
    display: ['none', 'flex'],
    justifyContent: 'center',
  })
);


export const ImageBanner = styled.div`
    width: 50%;
    background-size: contain;
    background-repeat: no-repeat, no-repeat;
    text-align: center;

    @media screen and (max-width: 581px) {
        width: 100%;
    }
  `

export const ImageBanner2 = styled.div`
display: none;

@media screen and (max-width: 581px) {
  display: block;
  width: 100%;
}
`

export const BodyBanner = styled.div`
    width: 40%;
    display: flex;
    align-items: center;
    
    @media screen and (max-width: 581px) {
      width: 100%;
      padding: 25px;
      text-align: center
    }
  `

export const BodyBannerContent = styled.div(
  css({
    display: 'block',
  })
);


export const BannerContainer = styled.div`
  display: flex;
  flexWrap: wrap;
  background-color: white;

  @media screen and (max-width: 581px) {
    display: block;
    text-align: center;
  }
`

export const BannerContainerAdvertisingResponsive = styled.div`
  padding: 15px 40px;
  background: white;
  @media (max-width: 581px) {
    padding: 15px 15px;
  }
`

export const BannerContainerAdvertising = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 230px;
  background-repeat: no-repeat no-repeat;
  background-size: cover;
`

export const Button = styled.div`
  width: fit-content;
  padding: 0 15px;
  color: white;
  margin-left: auto;
  margin-right: auto;
  background-color: #F9AC17;
  cursor: pointer;
  :hover {
    background-color: #e89b16;
  }
`
