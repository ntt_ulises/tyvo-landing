import React from 'react';
import imgAppStore from '../../assets/images/appstore.png';
import imgPhone from '../../assets/images/banner/phone.png';
import imgPlayStore from '../../assets/images/playstore.png';
import {ImageBanner, BodyBanner, BodyBannerContent, BannerContainer} from './banner.style';

const BannerGetMobilesApps = ({deviceType: {desktop}}: Props) => {
  return <BannerContainer style={{paddingTop: 30, paddingBottom: 30}}>
    <ImageBanner>
      <img width={!!desktop ? 200 : 150} src={imgPhone} alt="Phone"/>
    </ImageBanner>
    <BodyBanner>
      <BodyBannerContent>
        <h3>Disfruta de una mejor experiencia descargando la App</h3>
        <BannerContainer>
          <a target='_blank' rel='noopener noreferrer' href="https://www.apple.com/la/ios/app-store/">
            <img src={imgAppStore} alt="App Store" height={50}/>
          </a>
          <a target='_blank' rel='noopener noreferrer' href="https://play.google.com/store">
            <img src={imgPlayStore} alt="Play Store" height={50}/>
          </a>
        </BannerContainer>
      </BodyBannerContent>
    </BodyBanner>
    {/*<ImageBanner2>*/}
    {/*  <img width='150' src={imgPhone} alt="Phone" />*/}
    {/*</ImageBanner2>*/}
  </BannerContainer>
}

interface Props {
  deviceType: {
    // mobile: boolean
    // tablet: boolean
    desktop: boolean
  }
}

export default BannerGetMobilesApps