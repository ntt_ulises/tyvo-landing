import React from 'react';
import usePromoter from "../../data/use-promoter";
import {Grid, Row, Col} from "react-styled-flexboxgrid";
import {Button} from "../button/button";
import ReadMore, {TextTruncate} from "../truncate/truncate";

interface Props {

}

export const BannerPromoter: React.FC<Props> = () => {
  const {loading, data} = usePromoter()
  
  return <div style={{margin: 15}}>
    <Grid style={{width: '100%'}}>
      <Row style={{padding: 0}}>
        {
          data.map(({image, title, id, background, description, ok_button, text_color}) => {
            return <Col xs={12} sm={6} key={id} style={{paddingRight: 20}}>
              <Row style={{background, marginBottom: 20, overflow: 'hidden', height: 200}}>
                <Col xs={12} sm={6} style={{paddingLeft: 0, paddingRight: 0}}>
                  <img src={image} alt={title} width='100%' height='200px' style={{objectFit: 'cover'}}/>
                </Col>
                <Col xs={12} sm={6} style={{paddingTop: 20, paddingBottom: 20}}>
                  <p><strong>{title}</strong></p>
                  <TextTruncate style={{color: text_color}} character={75}>{description}</TextTruncate>
                  <Button type='button' style={{background: 'white', color: 'black', marginTop: 10}}>{ok_button}</Button>
                </Col>
              </Row>
            </Col>
          })
        }
      </Row>
    </Grid>
  </div>
};
