import React from 'react';
import Link from 'next/link';
import styles from './styles.module.css'
import {FormattedMessage} from 'react-intl';

const CarouselServices = ({data}: Props) => {
  return <>
    <h2 style={{textAlign: 'center'}}>
      <FormattedMessage
        id={'ourServices'}
        defaultMessage="Our Services"
      />
    </h2>
    <div className={styles.container}>
      {
        data.map(({background, id, imgSrc, title, link}: IServices) => {
          return <div key={id} className={styles.cardServices}>
            <Link href={link}>
              <a style={{cursor: 'pointer'}}>
                <div className={styles.cardServicesBody} style={{backgroundColor: background}}>
                  <img
                    src={imgSrc}
                    alt={title}
                    style={{
                      width: 93,
                      height: 70,
                      objectFit: 'contain',
                      marginTop: -20
                    }}
                  />
                  <div className={styles.cardTitle}>
                    <span>{title}</span>
                  </div>
                </div>
              </a>
            </Link>
          </div>
        })}
    </div>
  </>
}

export default CarouselServices

interface IServices {
  id: number
  imgSrc: string
  title: string
  link: string
  background:
    string
}

type Props = {
  data: any[] | undefined;
  // deviceType: {
  //   mobile: boolean;
  //   tablet: boolean;
  //   desktop: boolean;
  // };
};