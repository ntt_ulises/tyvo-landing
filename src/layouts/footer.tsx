import React from "react";
import {useMedia} from 'utils/use-media';
import {FormattedMessage} from 'react-intl';

const Footer = () => {
  const mobile = useMedia('(max-width: 580px)');
  const tablet = useMedia('(max-width: 991px)');
  const desktop = useMedia('(min-width: 992px)');
  
  return <footer>
    <div className='d-flex flex-wrap'>
      <div className='p-20' style={{width: `calc(100% / ${mobile ? 2 : tablet ? 3 : desktop && 5})`}}>
        <div className='p-20'/>
        <p className='text-secondary'>+503 0000-0000</p>
        <a href="#"><p>support@tyvo.io</p></a>
        <p>99 Wall St, Suite 1821, NYC 10005 USA / 27 a.v. poniente decima y 12 calle poniente #633 Colonia Flor Blanca,
          San Salvador, El Salvador</p>
      </div>
      <div className='p-20' style={{width: `calc(100% / ${mobile ? 2 : tablet ? 3 : desktop && 5})`}}>
        <p><b>Menu Rápido</b></p>
        <br/>
        <p>TV y Video</p>
      </div>
      <div className='p-20' style={{width: `calc(100% / ${mobile ? 2 : tablet ? 3 : desktop && 5})`}}>
        <p><b>Información</b></p>
      
      </div>
      <div className='p-20' style={{width: `calc(100% / ${mobile ? 2 : tablet ? 3 : desktop && 5})`}}>
        <p><b>Mi Cuenta</b></p>
      
      </div>
      <div className='p-20' style={{width: `calc(100% / ${mobile ? 2 : tablet ? 3 : desktop && 5})`}}>
        <p><b>NewsLetter</b></p>
      
      </div>
    </div>
    <div style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'center'}}>
      <span style={{marginRight: 20}}>
        <a href="#" style={{color: '#0B2749'}}>&copy; 2020 Tyvo</a>
      </span>
      <span style={{marginRight: 20}}>
        <a href="#" style={{color: '#0B2749'}}>
        <FormattedMessage
          id='siteFooterTerm'
          defaultMessage='Terms and Conditions'
        />
        </a>
      </span>
      <span>
        <a href="#" style={{color: '#0B2749'}}>
        <FormattedMessage
          id='siteFooterPolicies'
          defaultMessage='Privacy policies'
        />
        </a>
      </span>
    </div>
  </footer>
};
export default Footer;
