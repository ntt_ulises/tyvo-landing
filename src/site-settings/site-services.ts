import imgTransport from 'assets/images/services/transports.png';
import imgMarket from 'assets/images/services/market.png';
import imgShop from 'assets/images/services/shop.png';
import imgShipping from 'assets/images/services/shipping.png';
import imgCash from 'assets/images/services/cash.png';
import imgFood from 'assets/images/services/food.png';
import imgFarmacy from 'assets/images/services/farmacy.png';
import imgPets from 'assets/images/services/pets.png';
import imgDonations from 'assets/images/services/donations.png';


export const siteServices = [
  {
    id: '1',
    imgSrc: imgTransport,
    title: 'transport',
    link: '/',
    background: "#FFC0C4"
  },
  {
    id: '2',
    imgSrc: imgMarket,
    title: 'market',
    link: '/',
    background: "#FFE8AD"
  },
  {
    id: '3',
    imgSrc: imgShop,
    title: 'shops',
    link: '/',
    background: "#ADD1E8"
  },
  {
    id: '4',
    imgSrc: imgShipping,
    title: 'shipping',
    link: '/',
    background: "#FDECD7"
  },
  {
    id: '5',
    imgSrc: imgCash,
    title: 'cash',
    link: '/',
    background: "#D2E697"
  },
  {
    id: '6',
    imgSrc: imgFood,
    title: 'restaurant',
    link: '/',
    background: "#FFC0C4"
  },
  {
    id: '7',
    imgSrc: imgFarmacy,
    title: 'farmacy',
    link: '/',
    background: "#A8E3EE"
  },
  {
    id: '8',
    imgSrc: imgPets,
    title: 'pets',
    link: '/',
    background: "#A2DFCE"
  },
  {
    id: '9',
    imgSrc: imgDonations,
    title: 'donations',
    link: '/',
    background: "#C2EAF1"
  },
];
